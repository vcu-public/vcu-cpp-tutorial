#ifndef APPLICATION_H
#define APPLICATION_H

#include <omlib/rpc_application.h>

namespace om
{

class Application : public RPCApplication
{
    protected:
        bool prerun() override;

        void afterRPCActive() override;

        void teardown() override;

        void setupCmdlineArguments();

        void rpcLogCallback(RedisRPCRequestPtr req);

    public:
        Application(int argc, char **argv);

        ~Application() override = default;
};

}

#endif // APPLICATION_H
