#include "application.h"

int main(int argc, char **argv)
{
    om::Application app(argc, argv);
    app.run();
    return 0;
}
