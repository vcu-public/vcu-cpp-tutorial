#include <fstream>

#include "application.h"

using namespace std::placeholders;

om::Application::Application(int argc, char **argv)
    : RPCApplication(PROJECT_NAME, PROJECT_VER, CATEGORY_APP_PREFIX + std::string(PROJECT_NAME))
{
    setupCmdlineArguments();
    // Parsing command line arguments
    cmdLineParser.parse(argc, argv);
}


bool om::Application::prerun() {
    // call the parent's implementation to process generic command-line arguments
    if (!RPCApplication::prerun()) {
        return false;
    }
    auto &text = cmdLineParser.getValues().vm["log-option"].as<std::string>();

    log().info("Your text is {}", text);
    return true;
}


void om::Application::rpcLogCallback(om::RedisRPCRequestPtr req)
{
    auto &text = req->getArgs()["text"].getString();
    log().warn("RPC command called with text: {}", text.data());
    req->sendResponse("printed");
}


void om::Application::afterRPCActive()
{
    getMainEndpoint()->registerCommand("logCommand", {"+text:s"}, std::bind(&Application::rpcLogCallback, this, _1));
}


void om::Application::teardown() {
    log().info("Bye world");
    RPCApplication::teardown();
}


void om::Application::setupCmdlineArguments()
{
    namespace po = boost::program_options;
    po::options_description desc;
    desc.add_options()
            ("log-option,o", po::value<std::string>()->required(), "Text to write in the log");
    cmdLineParser.addCmdLineOption(desc);
}
