#include <fstream>

#include "application.h"

using namespace std::placeholders;


constexpr char hexmap[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                           '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

// Prints value to output stream in hex form
static
void stringToHex(std::ostream &buffer, const std::string &value)
{
    for(auto &byte : value) {
        buffer << "\\x";
        buffer << hexmap[(byte & 0xF0) >> 4];
        buffer << hexmap[(byte & 0x0F)];
    }
}


om::Application::Application(int argc, char **argv)
    : RPCApplication(PROJECT_NAME, PROJECT_VER, CATEGORY_APP_PREFIX + std::string(PROJECT_NAME))
{
    setupCmdlineArguments();
    // Parsing command line arguments
    cmdLineParser.parse(argc, argv);

    // Reading 'config-file' parameter
    std::string configFileName = cmdLineParser.getValues().vm["config-file"].as<std::string>();

    try {
        config = ConfigParser::parseConfiguration(configFileName);
    } catch (ParseError &err) {
        log().critical(err.what());
        ::exit(EXIT_CONFIGERR);
    }

    // Override default parameters values
    // The following command line arguments come with default values
    // We want to override these values with values from the config file
    // only when no value has been explicitly set on the command line
    cmdLineParser.overrideDefaultedValue("redis-uri", config.redisURI);
    cmdLineParser.overrideDefaultedValue("loglevel", config.logLevel);
}


bool om::Application::prerun() {
    // call the parent's implementation to process generic command-line arguments
    if (!RPCApplication::prerun()) {
        return false;
    }
    return true;
}


void om::Application::afterRPCActive()
{
    // connecting to CAN sources
    // Application::onClientStatusChangeCallback - called everytime the connection state of the source changes
    // Application::onIncomingFrame - called everytime a new CAN frame arrives
    for (auto &can : config.canConfig) {
        auto source = std::make_unique<CANSource>(can.source,
                                                  getMainEndpoint(),
                                                  std::bind(&Application::onClientStatusChangeCallback, this, _1, _2),
                                                  std::bind(&Application::onIncomingFrame, this, _1, _2, _3));
        // Creating JSON arguments from filter value
        JSONObject args;
        JSONObject o;
        for (auto &item : can.filter) {
            o[item.first] = JSONValue::fromStringArray(item.second);
        }
        args["filter"] = o;

        source->connect(std::move(args));

        // starts the source - unplugs data processing
        // can be used to synchronize multiple sources if scheduled properly
        source->run();
        sources.push_back(std::move(source));
    }
}


void om::Application::teardown() {
    log().info("Stopping all sources");
    for (auto &source : sources) {
        source->disconnect();
    }
    RPCApplication::teardown();
}


void om::Application::setupCmdlineArguments()
{
    namespace po = boost::program_options;
    po::options_description desc;
    desc.add_options()
            ("config-file,f", po::value<std::string>()->required(), "Path to configuration file");
    cmdLineParser.addCmdLineOption(desc);
}


void om::Application::onClientStatusChangeCallback(const std::string &source, const boost::system::error_code &error)
{
    if (error) {
        if (error == boost::asio::error::connection_refused) {
            // connection has been refused - wrong RPC parameters, check server log
        }

        if (error == boost::asio::error::timed_out) {
            // connection has timed out - no RPC server present
        }

        if (error == boost::asio::error::connection_aborted) {
            // connection aborted - server disappeared, source will try to reconnect itself
        }

        log().error("Source {} cannot connect, quitting", source);
        quit(EXIT_FAILURE);
        return;
    }
    log().info("Source {} connected", source);
}


void om::Application::onIncomingFrame(const std::string &source, om::TimestampType timestamp,
                                      CanFrames::CanFrame &frame)
{
    auto &buffer = std::cerr;
    buffer << '"' << source << "\",\"" << timestamp << "\",\"";
    std::ios_base::fmtflags f(buffer.flags());
    buffer << std::hex << std::showbase << frame.id() << "\",\"";
    buffer.flags(f);
    stringToHex(buffer, frame.data());
    buffer << "\"\n";
}
