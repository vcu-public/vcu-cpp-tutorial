#ifndef CAN_SOURCE_H
#define CAN_SOURCE_H

#include <functional>

#include <omlib/dynamic_source.h>
#include <protobuf/can_frames.pb.h>

#include "application.h"

namespace om
{

using CANDataHandler = std::function<void(const std::string &source, TimestampType timetamp, CanFrames::CanFrame &frame)>;

class CANSource : public DynamicSource
{
    private:
        CANDataHandler dataHandler;

    protected:

        virtual void parseMessage(const om::ByteArray &msg) override
        {
            CanFrames recs;
            if (!recs.ParseFromString(msg)) {
                log().warn("Cannot parse msg from {}", source);
                return;
            }
            TimestampType first = recs.timestamp();
            for (auto &rec : *recs.mutable_frames()) {
                auto timestamp = first + rec.timedelta();
                dataHandler(source, timestamp, rec);
            }
            log().trace("Got {} frames in {}", recs.frames().size(), source);
        }

    public:
        CANSource(const std::string &sourceName, RedisRPCPtr endpointptr,
                  SourceStateHandler stateHandler, CANDataHandler dataHandler)
            : DynamicSource(sourceName, endpointptr, stateHandler),
              dataHandler(dataHandler)
        {
        }

        virtual ~CANSource() override = default;
};

} // namespace om

#endif // CAN_SOURCE_H
