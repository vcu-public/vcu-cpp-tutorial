#ifndef CONFIG_PARSER_H
#define CONFIG_PARSER_H

#include <string>
#include <vector>
#include <map>

#include <omlib/common.h>

namespace om
{

struct CANConfig
{
        std::string source;
        std::map<std::string, std::vector<std::string>> filter;
};

struct ApplicationConfiguration
{
        std::string redisURI;
        std::string logLevel;
        std::vector<CANConfig> canConfig;
};


class ConfigParser
{
    private:
        ConfigParser() {}

    public:
        static ApplicationConfiguration parseConfiguration(const std::string &filePath);
};

} //namespace om

#endif // CONFIG_PARSER_H
