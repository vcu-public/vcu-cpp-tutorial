#ifndef APPLICATION_H
#define APPLICATION_H

#include <functional>

#include <omlib/rpc_application.h>

#include "config_parser.h"
#include "can_source.h"

namespace om
{

class Application : public RPCApplication
{
    private:
        ApplicationConfiguration config;

        std::vector<std::unique_ptr<DataSource>> sources;

    protected:
        bool prerun() override;

        void afterRPCActive() override;

        void teardown() override;

        void setupCmdlineArguments();

        void onClientStatusChangeCallback(const std::string &source, const boost::system::error_code &error);

        void onIncomingFrame(const std::string &source, TimestampType timestamp, CanFrames::CanFrame &frame);

    public:
        Application(int argc, char **argv);

        ~Application() override = default;
};

}

#endif // APPLICATION_H
