#ifndef APPLICATION_H
#define APPLICATION_H

#include <functional>

#include <omlib/rpc_application.h>
#include <omlib/periodic_timer.h>

#include "protobuf/cantx_frames.pb.h"


namespace om
{

class Application : public RPCApplication
{
    private:
        std::unique_ptr<RedisRPCClient> writer;

        PeriodicTimer writeTimer;
        int frameCounter;

    protected:
        bool prerun() override;

        void afterRPCActive() override;

        void teardown() override;

        void setupCmdlineArguments();

        void onWriterStatusChange(const boost::system::error_code &error);

    public:
        Application(int argc, char **argv);

        ~Application() override = default;
};

}

#endif // APPLICATION_H
