#include <fstream>

#include "application.h"

using namespace std::placeholders;
using namespace std::chrono_literals;

static const auto CAN_WRITER_INSTANCE = "cantx@vcan0";

om::Application::Application(int argc, char **argv)
    : RPCApplication(PROJECT_NAME, PROJECT_VER, CATEGORY_APP_PREFIX + std::string(PROJECT_NAME)),
      writeTimer(loop, "writeTimer")
{
    setupCmdlineArguments();
    // Parsing command line arguments
    cmdLineParser.parse(argc, argv);
}


bool om::Application::prerun() {
    // call the parent's implementation to process generic command-line arguments
    if (!RPCApplication::prerun()) {
        return false;
    }
    return true;
}


void om::Application::afterRPCActive()
{
    // create a redis client instance
    writer = std::make_unique<RedisRPCClient>();

    // connect writer to the Application's main endpoint
    writer->registerToEndpoint(getMainEndpoint());

    writer->registerRPCSender(cmdLineParser.getValues().vm["write-target"].as<std::string>(),
                              JSONObject{{"saeTP", true}},
                              std::bind(&Application::onWriterStatusChange, this, _1));
}


void om::Application::teardown() {
    if (writer) {
        writer->unregisterRPCSender();
    }
    writeTimer.close();
    RPCApplication::teardown();
}


void om::Application::setupCmdlineArguments()
{
    namespace po = boost::program_options;
    po::options_description desc;
    desc.add_options()
            ("write-target,w", po::value<std::string>()->default_value(CAN_WRITER_INSTANCE), "CAN writer address");
    cmdLineParser.addCmdLineOption(desc);
}


void om::Application::onWriterStatusChange(const boost::system::error_code &error)
{
    if (error) {
        if (error == boost::asio::error::connection_refused) {
            // connection has been refused - wrong RPC parameters, check server log
        }

        if (error == boost::asio::error::timed_out) {
            // connection has timed out - no RPC server present
        }

        if (error == boost::asio::error::connection_aborted) {
            // connection aborted - server disappeared, writer will try to reconnect itself
        }

        log().error("CAN writer cannot connect, quitting");
        quit(EXIT_FAILURE);
        return;
    }
    log().info("CAN writer connected");

    // send 5 CAN frames and exit application
    frameCounter = 0;
    writeTimer.run(200ms, [this]() mutable {
        CanTXFrames frames;
        auto *frame = frames.add_frames();
        if (frame == nullptr) {
            throw std::bad_alloc();
        }
        // set CAN ID
        frame->set_id(0x123);
        // by default, the ID is considered an extended ID, use this flag to denote base ID
        frame->set_flags(CanTXFrames_CanTXFrame_CanFrameFlags_BASEID);
        // up to 8 bytes for a standard frame, if longer data is sent, depending on the client
        // parameters and the adapter's configuration, a SAE TP BAM may be used to send it (see afterRPCActive())
        frame->set_data("\x08\x09\xaa\xbb\xcc\xdd\xee\xff");

        log().info("Sending CAN frame");
        redis.publish(writer->getSenderTopic(), frames.SerializeAsString());

        if (frameCounter++ >= 5) {
            quit(EXIT_SUCCESS);
        }
    });
}

