#include <fstream>

#include <omlib/common.h>
#include <omlib/global_config.h>
#include <omlib/config_functions.h>

#include "config_parser.h"

static
std::vector<std::string> parseAndCheckCANFilterIDs(YAML::Node &filter)
{
    std::vector<std::string> res;
    if (filter.size() == 0) {
        throwParseError(filter, "Filter id is not an array");
    }
    for (size_t i = 0; i < filter.size(); i++) {
        std::string tmp = getValueFromNode<std::string>(filter[i]);

        if (tmp.empty()) {
            throwParseError(filter[i], "Filter cannot be empty");
        }
        long long valueToCheck;
        if (tmp.at(0) == 'b') {
            if (!om::hexStringToNumber(tmp.c_str() + 1, valueToCheck)) {
                throwParseError(filter[i], "Invalid CAN filter id");
            }
            if (valueToCheck > CAN_ID_MAX_VALUE || valueToCheck < 0) {
                throwParseError(filter[i], "Filter id must be between 0 and " STRINGIFY(CAN_ID_MAX_VALUE));
            }
        } else {
            if (!om::hexStringToNumber(tmp.c_str(), valueToCheck)) {
                throwParseError(filter[i], "Invalid CAN filter id");
            }
            if (valueToCheck > CAN_EID_MAX_VALUE || valueToCheck < 0) {
                throwParseError(filter[i], "Filter id must be between 0 and " STRINGIFY(CAN_EID_MAX_VALUE));
            }
        }
        res.push_back(tmp);
    }
    return res;
}


static
om::CANConfig parseCANSourceFields(YAML::Node &fieldsNode)
{
    om::CANConfig tmp;
    auto filterNode = parseRequiredSection(fieldsNode, "filter");
    // node type check
    if (filterNode.IsSequence()) {
        std::vector<std::string> tmpFilters = parseAndCheckCANFilterIDs(filterNode);
        tmp.filter.emplace(om::numberToHexString(CAN_EID_MAX_VALUE), tmpFilters);
    } else if (filterNode.IsMap()) {
        // parsing CAN filter in form {"<hex mask>" : ["<hex CAN id>", "<hex CAN id>", ...]}
        for (YAML::iterator ft = filterNode.begin(); ft != filterNode.end(); ++ft) {
            std::string mask = getValueFromNode<std::string>(ft->first);
            long long valueToCheck;
            if (!om::hexStringToNumber(mask, valueToCheck)) {
                throwParseError(ft->first, "Invalid filter mask");
            }
            if (valueToCheck > CAN_EID_MAX_VALUE) {
                throwParseError(ft->first, "CAN mask is too large");
            }
            std::vector<std::string> tmpFilters = parseAndCheckCANFilterIDs(ft->second);
            tmp.filter.emplace(mask, tmpFilters);
        }
    } else if (filterNode.IsScalar()) {
        std::string value = boost::algorithm::to_lower_copy(getValueFromNode<std::string>(filterNode));
        if (value != "all") {
            throwInvalidValue(filterNode, "Expecting 'all'");
        }
        tmp.filter.emplace("0x0", std::vector<std::string>{"0x0"});
    } else {
        throwUnexpectedType(filterNode, "Expecting a map of filters or 'all'");
    }
    return tmp;
}


om::ApplicationConfiguration om::ConfigParser::parseConfiguration(const std::string &filePath)
{
    ApplicationConfiguration out;
        char *cwd = nullptr;
        std::string fullPath = filePath;
        if (filePath[0] != '/') {
            if ((cwd = get_current_dir_name()) == nullptr) {
                throwParseError(-1, -1, "Cannot obtain current working directory: {}", ::strerror(errno));
            }
            fullPath = std::string(cwd) + '/' + filePath;
            free(cwd);
        }

        if (::access(fullPath.c_str(), R_OK) < 0) {
            throwParseError(-1, -1, "Configuration file {} is not accessible: {}", fullPath, ::strerror(errno));
        }

        YAML::Node config;
        try {
            std::ifstream ifs(fullPath);
            // Loading YAML configuration
            config = YAML::Load(ifs);
        } catch (YAML::ParserException &err) {
            throwParseError(err.mark.line, err.mark.column, err.msg.c_str());
        }

        // parsing log-level parameter(ex: log-level: debug)
        out.logLevel = parseOptionalChoiceKey(config, "log-level", Logger::getLogLevels(), DEFAULT_LOG_LEVEL);

        // parsing Redis URI
        out.redisURI = parseOptionalKey<std::string>(config, "redis-uri", DEFAULT_REDIS_URI);

        //SOURCES
        YAML::Node sources = parseRequiredKey<YAML::Node>(config, "sources");
        if (sources.size() == 0) {
            throwParseError(sources, "Required section 'sources' is empty");
        }

        // Iterating through source nodes
        for (YAML::iterator it = sources.begin(); it != sources.end(); ++it) {
               std::string sourceLine = getValueFromNode<std::string>(it->first);
               size_t atPos = sourceLine.find('@');
               if (atPos == std::string::npos) {
                   throwInvalidValue(it->first, "Wrong source format, expecting <category>@<resource>");
               }
               std::string category = sourceLine.substr(0, atPos);
               std::string source = sourceLine.substr(atPos + 1);
               if (category == om::CATEGORY_CANRX) {
                   auto source = parseCANSourceFields(it->second);
                   source.source = std::move(sourceLine);
                   out.canConfig.push_back(std::move(source));
               }
           }
        return out;
}
