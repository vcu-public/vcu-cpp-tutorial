#include <fstream>

#include <omlib/common.h>
#include <omlib/global_config.h>
#include <omlib/config_functions.h>

#include "config_parser.h"

static
std::vector<std::string> parseAndCheckCANFilterIDs(YAML::Node &filter)
{
    std::vector<std::string> res;
    if (filter.size() == 0) {
        throwParseError(filter, "Filter id is not an array");
    }
    for (auto &&node : filter) {
        std::string id = getValueFromNode<std::string>(node);

        if (id.empty()) {
            throwParseError(node, "Filter cannot be empty");
        }
        std::string::difference_type n = std::count(id.begin(), id.end(), '@');
        if (n > 1) {
            throwParseError(node, "Invalid CAN signal id");
        }
        res.push_back(std::move(id));
    }
    return res;
}


static
om::CANSignalConfig parseCANSignalSourceFields(YAML::Node &fieldsNode)
{
    om::CANSignalConfig tmp;
    auto filterNode = parseRequiredSection(fieldsNode, "signals");
    // node type check
    if (filterNode.IsSequence()) {
        tmp.signals = parseAndCheckCANFilterIDs(filterNode);
    } else if (filterNode.IsScalar()) {
        std::string value = boost::algorithm::to_lower_copy(getValueFromNode<std::string>(filterNode));
        if (value != "all") {
            throwInvalidValue(filterNode, "Expecting 'all'");
        }
    } else {
        throwUnexpectedType(filterNode, "Expecting an array of filters or 'all'");
    }
    return tmp;
}


om::ApplicationConfiguration om::ConfigParser::parseConfiguration(const std::string &filePath)
{
    ApplicationConfiguration out;
    char *cwd = nullptr;
    std::string fullPath = filePath;
    if (filePath[0] != '/') {
        if ((cwd = get_current_dir_name()) == nullptr) {
            throwParseError(-1, -1, "Cannot obtain current working directory: {}", ::strerror(errno));
        }
        fullPath = std::string(cwd) + '/' + filePath;
        free(cwd);
    }

    if (::access(fullPath.c_str(), R_OK) < 0) {
        throwParseError(-1, -1, "Configuration file {} is not accessible: {}", fullPath, ::strerror(errno));
    }

    YAML::Node config;
    try {
        std::ifstream ifs(fullPath);
        // Loading YAML configuration
        config = YAML::Load(ifs);
    } catch (YAML::ParserException &err) {
        throwParseError(err.mark.line, err.mark.column, err.msg.c_str());
    }

    // parsing log-level parameter(ex: log-level: debug)
    out.logLevel = parseOptionalChoiceKey(config, "log-level", Logger::getLogLevels(), DEFAULT_LOG_LEVEL);

    // parsing Redis URI
    out.redisURI = parseOptionalKey<std::string>(config, "redis-uri", DEFAULT_REDIS_URI);

    //SOURCES
    YAML::Node sources = parseRequiredKey<YAML::Node>(config, "sources");
    if (sources.size() == 0) {
        throwParseError(sources, "Required section 'sources' is empty");
    }

    // Iterating through source nodes
    for (YAML::iterator it = sources.begin(); it != sources.end(); ++it) {
       std::string sourceLine = getValueFromNode<std::string>(it->first);

       size_t atPos = sourceLine.find('@');
       if (atPos == std::string::npos) {
           throwInvalidValue(it->first, "Wrong source format, expecting <category>@<resource>");
       }
       std::string category = sourceLine.substr(0, atPos);
       if (category == om::CATEGORY_CANSIGNALRX) {
           auto source = parseCANSignalSourceFields(it->second);
           source.source = std::move(sourceLine);
           out.canSignalConfig.push_back(std::move(source));
       }
    }
    return out;
}
