#ifndef SIGNAL_SOURCE_H
#define SIGNAL_SOURCE_H

#include <functional>

#include <omlib/dynamic_source.h>
#include <protobuf/cansignal_frames.pb.h>

#include "application.h"

namespace om
{

using CANSignalNames = std::vector<std::string>;

using CANSignalDataHandler = std::function<void(const std::string &source,
                                                TimestampType timetamp,
                                                const SignalMessages::SignalMessage::SignalFrame &frame,
                                                const CANSignalNames& names)>;

class CANSignalSource : public DynamicSource
{
    private:
        CANSignalDataHandler dataHandler;
        CANSignalNames signalNames;

    protected:

        virtual bool processRPCReply(const RPCReplyData &reply) override
        {
            signalNames.clear();
            if (!reply.contains("signals") || !reply["signals"].isArray()) {
                log().error("Invalid reply from CAN signal adapter {}", source);
                return false;
            }
            std::string name;
            for (auto &signal : reply["signals"].getArray()) {
                if (signal.contains("name")) {
                    name = signal["name"].getString().to_string();
                } else {
                    name = "unknown";
                }
                signalNames.push_back(name);
            }
            return true;
        }

        virtual void parseMessage(const om::ByteArray &msg) override
        {
            SignalMessages recs;
            if (!recs.ParseFromString(msg)) {
                log().warn("Cannot parse msg from {}", source);
                return;
            }
            TimestampType first = recs.timestamp();
            for (auto &message : *recs.mutable_messages()) {
                auto timestamp = first + message.timedelta();
                for (const auto &frame : message.frames()) {
                    dataHandler(source, timestamp, frame, signalNames);
                }
            }
            log().trace("Got {} frames from {}", recs.messages().size(), source);
        }

    public:
    CANSignalSource(const std::string &sourceName, RedisRPCPtr endpointptr,
                  SourceStateHandler stateHandler, CANSignalDataHandler dataHandler)
            : DynamicSource(sourceName, endpointptr, stateHandler),
              dataHandler(dataHandler)
        {
        }

        virtual ~CANSignalSource() override = default;
};

} // namespace om

#endif // SIGNAL_SOURCE_H
