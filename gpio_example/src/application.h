#ifndef APPLICATION_H
#define APPLICATION_H

#include <functional>

#include <omlib/rpc_application.h>
#include <omlib/periodic_timer.h>

#include "config_parser.h"
#include "gpio_source.h"

namespace om
{

class Application : public RPCApplication
{
    private:
        ApplicationConfiguration config;

        std::unique_ptr<GPIOSource> source;

        std::unique_ptr<RedisRPCClient> gpioTarget;

        PeriodicTimer targetTimer;

        bool gpoState;

    protected:
        bool prerun() override;

        void afterRPCActive() override;

        void teardown() override;

        void setupCmdlineArguments();

        void onSourceStatusChangeCallback(const std::string &source, const boost::system::error_code &error);

        void onTargetStatusChangeCallback(const boost::system::error_code &error);

        void onIncomingFrame(TimestampType timestamp, const GpiFrames::GpiFrame &frame);

    public:
        Application(int argc, char **argv);

        ~Application() override = default;
};

}

#endif // APPLICATION_H
