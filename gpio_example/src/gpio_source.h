#ifndef CAN_SOURCE_H
#define CAN_SOURCE_H

#include <functional>

#include <omlib/dynamic_source.h>
#include <protobuf/gpio_frames.pb.h>

#include "application.h"

namespace om
{

using GPIODataHandler = std::function<void(TimestampType timetamp, const GpiFrames::GpiFrame&)>;

class GPIOSource : public DynamicSource
{
    private:
        GPIODataHandler dataHandler;

    protected:

        virtual void parseMessage(const om::ByteArray &msg) override
        {
            GpiFrames recs;
            if (!recs.ParseFromString(msg)) {
                log().warn("Cannot parse msg from {}", source);
                return;
            }
            TimestampType first = recs.timestamp();
            for (auto &rec : *recs.mutable_frames()) {
                auto timestamp = first + rec.timedelta();
                dataHandler(timestamp, rec);
            }
            log().trace("Got {} frames in {}", recs.frames().size(), source);
        }

    public:
        GPIOSource(const std::string &sourceName, RedisRPCPtr endpointptr,
                  SourceStateHandler stateHandler, GPIODataHandler dataHandler)
            : DynamicSource(sourceName, endpointptr, stateHandler),
              dataHandler(dataHandler)
        {
        }

        virtual ~GPIOSource() override = default;
};

} // namespace om

#endif // CAN_SOURCE_H
