#include <fstream>

#include "application.h"

using namespace std::placeholders;
using namespace std::chrono_literals;


om::Application::Application(int argc, char **argv)
    : RPCApplication(PROJECT_NAME, PROJECT_VER, CATEGORY_APP_PREFIX + std::string(PROJECT_NAME)),
      targetTimer(loop, "gpioTargetTimer"),
      gpoState(false)
{
    setupCmdlineArguments();
    // Parsing command line arguments
    cmdLineParser.parse(argc, argv);

    // Reading 'config-file' parameter
    std::string configFileName = cmdLineParser.getValues().vm["config-file"].as<std::string>();

    try {
        config = ConfigParser::parseConfiguration(configFileName);
    } catch (ParseError &err) {
        log().critical(err.what());
        ::exit(EXIT_CONFIGERR);
    }

    // Override default parameters values
    // The following command line arguments come with default values
    // We want to override these values with values from the config file
    // only when no value has been explicitly set on the command line
    cmdLineParser.overrideDefaultedValue("redis-uri", config.redisURI);
    cmdLineParser.overrideDefaultedValue("loglevel", config.logLevel);
}


bool om::Application::prerun() {
    // call the parent's implementation to process generic command-line arguments
    if (!RPCApplication::prerun()) {
        return false;
    }
    return true;
}


void om::Application::afterRPCActive()
{
    // connecting the GPIO source
    // Application::onClientStatusChangeCallback - called everytime the connection state of the source changes
    // Application::onIncomingFrame - called everytime a new GPI frame arrives
    source = std::make_unique<GPIOSource>(config.source,
                                          getMainEndpoint(),
                                          std::bind(&Application::onSourceStatusChangeCallback, this, _1, _2),
                                          std::bind(&Application::onIncomingFrame, this, _1, _2));
    // Creating JSON arguments from filter value
    JSONObject args;
    JSONArray a;
    for (auto &item : config.inputFilter) {
        a.push_back(item);
    }
    args["filter"] = a;

    source->connect(std::move(args));

    // starts the source - unplugs data processing
    // can be used to synchronize multiple sources if scheduled properly
    source->run();

    // create sender connection, we need to use RedisRPCClient directly as there is no target abstraction
    gpioTarget = std::make_unique<RedisRPCClient>();

    // connect client to our main endpoint
    gpioTarget->registerToEndpoint(getMainEndpoint());

    // we send data to the adapter here since we want to control GPOs, hence registerRPCSender()
    gpioTarget->registerRPCSender(config.writeTarget, RPCNoArguments,
                                  std::bind(&Application::onTargetStatusChangeCallback, this, _1));

}


void om::Application::teardown() {
    log().info("Stopping application");
    if (source) {
        source->disconnect();
    }
    if (gpioTarget) {
        gpioTarget->unregisterRPCSender();
    }
    targetTimer.close();
    RPCApplication::teardown();
}


void om::Application::setupCmdlineArguments()
{
    namespace po = boost::program_options;
    po::options_description desc;
    desc.add_options()
            ("config-file,f", po::value<std::string>()->required(), "Path to configuration file");
    cmdLineParser.addCmdLineOption(desc);
}


void om::Application::onSourceStatusChangeCallback(const std::string &source, const boost::system::error_code &error)
{
    if (error) {
        if (error == boost::asio::error::connection_refused) {
            // connection has been refused - wrong RPC parameters, check server log
        }

        if (error == boost::asio::error::timed_out) {
            // connection has timed out - no RPC server present
        }

        if (error == boost::asio::error::connection_aborted) {
            // connection aborted - server disappeared, source will try to reconnect itself
        }

        log().error("Source {} cannot connect, quitting", source);
        quit(EXIT_FAILURE);
        return;
    }
    log().info("Source {} connected", source);
}


void om::Application::onTargetStatusChangeCallback(const boost::system::error_code &error)
{
    if (error) {
        if (error == boost::asio::error::connection_refused) {
            // connection has been refused - wrong RPC parameters, check server log
        }

        if (error == boost::asio::error::timed_out) {
            // connection has timed out - no RPC server present
        }

        if (error == boost::asio::error::connection_aborted) {
            // connection aborted - server disappeared, target will try to reconnect itself
        }

        log().error("GPIO target cannot connect, quitting");
        quit(EXIT_FAILURE);
        return;
    }
    log().info("GPIO target connected");

    // let's turn two GPO pins on and off every second with opposite polarity
    targetTimer.run(1s,
                    [this]() {
        GpoFrames frames;
        auto frame1 = frames.add_frames();
        auto frame2 = frames.add_frames();
        if (frame1 == nullptr || frame2 == nullptr) {
            throw std::bad_alloc();
        }
        gpoState = !gpoState;
        frame1->set_channel(0);
        frame1->set_value(gpoState);
        frame2->set_channel(1);
        frame2->set_value(!gpoState);
        redis.publish(gpioTarget->getSenderTopic(), frames.SerializeAsString());
    });
}


void om::Application::onIncomingFrame(om::TimestampType timestamp,
                                      const GpiFrames::GpiFrame &frame)
{
    auto &buffer = std::cerr;
    // prints timestamp, GPI channel, logic value, and RAW voltage in mV
    buffer << '"' << timestamp << "\",\"";
    buffer << "gpi" << frame.channel() << "\",\"" << frame.value() << "\",\"" << frame.rawvalue() << "\"\n";
}
