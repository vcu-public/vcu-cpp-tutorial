#include <fstream>

#include <omlib/common.h>
#include <omlib/global_config.h>
#include <omlib/config_functions.h>

#include "config_parser.h"


om::ApplicationConfiguration om::ConfigParser::parseConfiguration(const std::string &filePath)
{
    ApplicationConfiguration out;
        char *cwd = nullptr;
        std::string fullPath = filePath;
        if (filePath[0] != '/') {
            if ((cwd = get_current_dir_name()) == nullptr) {
                throwParseError(-1, -1, "Cannot obtain current working directory: {}", ::strerror(errno));
            }
            fullPath = std::string(cwd) + '/' + filePath;
            free(cwd);
        }

        if (::access(fullPath.c_str(), R_OK) < 0) {
            throwParseError(-1, -1, "Configuration file {} is not accessible: {}", fullPath, ::strerror(errno));
        }

        YAML::Node config;
        try {
            std::ifstream ifs(fullPath);
            // Loading YAML configuration
            config = YAML::Load(ifs);
        } catch (YAML::ParserException &err) {
            throwParseError(err.mark.line, err.mark.column, err.msg.c_str());
        }

        // parsing log-level parameter(ex: log-level: debug)
        out.logLevel = parseOptionalChoiceKey(config, "log-level", Logger::getLogLevels(), DEFAULT_LOG_LEVEL);

        // parsing Redis URI
        out.redisURI = parseOptionalKey<std::string>(config, "redis-uri", DEFAULT_REDIS_URI);

        // source parsing
        std::string sourceLine = parseRequiredKey<std::string>(config, "source");
        size_t atPos = sourceLine.find('@');
        if (atPos == std::string::npos) {
            throwInvalidValue(config["source"], "Wrong source format, expecting <category>@<resource>");
        }
        std::string category = sourceLine.substr(0, atPos);
        if (category == om::CATEGORY_GPIORX) {
            out.source = std::move(sourceLine);
            out.inputFilter = parseRequiredKey<std::vector<int>>(config, "filter");
        }

        // target parsing
        std::string targetLine = parseRequiredKey<std::string>(config, "target");
        atPos = targetLine.find('@');
        if (atPos == std::string::npos) {
            throwInvalidValue(config["target"], "Wrong target format, expecting <category>@<resource>");
        }
        category = targetLine.substr(0, atPos);
        if (category == om::CATEGORY_GPIOTX) {
            out.writeTarget = std::move(targetLine);
        }

        return out;
}
