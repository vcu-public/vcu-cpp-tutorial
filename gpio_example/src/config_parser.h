#ifndef CONFIG_PARSER_H
#define CONFIG_PARSER_H

#include <string>
#include <vector>
#include <map>

#include <omlib/common.h>

namespace om
{

struct ApplicationConfiguration
{
        std::string redisURI;
        std::string logLevel;
        std::string source;
        std::vector<int> inputFilter;
        std::string writeTarget;
};


class ConfigParser
{
    private:
        ConfigParser() {}

    public:
        static ApplicationConfiguration parseConfiguration(const std::string &filePath);
};

} //namespace om

#endif // CONFIG_PARSER_H
