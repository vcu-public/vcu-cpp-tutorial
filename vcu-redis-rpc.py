#!/usr/bin/env python3

__author__      = "David Fabian"
__copyright__   = "Copyright 2020, ZF Openmatics"

import os
import sys
import time
import logging
import argparse
import json
import redis


class RedisConnector():

    def __init__(self):
        self._conn = None
        self._ps = None
        self._counter = 1
        self.name = 'x-app-vcu-redis-rpc-{}'.format(os.getpid())
        self._reply = '/vcu/rpc/{}/reply/v1'.format(self.name)


    def connect(self, uri):
        try:
            try:
                proto, rest = uri.split(':', 1)
            except ValueError:
                raise
            if proto == 'tcp':
                try:
                    host, port = rest.split(':', 1)
                    port = int(port)
                except ValueError:
                    raise RuntimeError('Invalid redis URI {}'.format(uri))
                self._conn = redis.Redis(host=host, port=port, decode_responses=True)
            elif proto == 'unix':
                self._conn = redis.Redis(unix_socket_path=rest, decode_responses=True)
            else:
                raise RuntimeError('Unknown protocol {} in redis URI'.format(proto))
        except redis.exceptions.RedisError as err:
            raise RuntimeError('Cannot connect to redis: {}'.format(str(err)))

        self._setup_pubsub()


    def _build_request(self, peer: str, counter: int, cmd: str, **kwargs):
        topic = '/vcu/rpc/{}/req/v1'.format(peer)
        out = {'cmd' : cmd,
               'id' : counter,
               'from' : self.name,
               'args' : kwargs}
        return (topic, out)


    def _setup_pubsub(self):
        self._ps = self._conn.pubsub(ignore_subscribe_messages=True)
        self._ps.subscribe(self._reply)


    def disconnect(self):
        if self._conn is None:
            return
        self._conn.close()
        self._conn = None


    def send_request(self, peer: str, cmd: str, **kwargs):
        id = self._counter
        topic, command = self._build_request(peer, id, cmd, **kwargs)
        self._counter += 1
        self._conn.publish(topic, json.dumps(command))
        start = time.time()
        while True:
            if (time.time() - start) > 10:
                raise RuntimeError('Connection timed-out, no reply from peer')
            message = self._ps.get_message()
            if message is None:
                time.sleep(0.2)
                continue
            if message['channel'] != self._reply:
                continue
            try:
                data = json.loads(message['data'])
            except ValueError:
                raise RuntimeError('Invalid data received from peer, cannot parse JSON')
            if data['id'] != id:
                continue
            if 'reply' in data:
                return (data['code'], data['reply'])
            else:
                return (data['code'], dict())


def parse_args():
    parser = argparse.ArgumentParser(description='Performs a single Redis RPC request')
    parser.add_argument('-L', '--loglevel', type=str, dest='loglevel', default='info', choices=['debug', 'info', 'warn', 'error', 'critical'],
                       help='Log level')
    parser.add_argument('-R', '--redis-uri', type=str, dest='uri', default='tcp:127.0.0.1:6379',
                       help='Redis connection string')
    parser.add_argument('-d', '--destination', type=str, dest='dest', required=True,
                       help='Name of the RPC endpoint to connect to')
    parser.add_argument('-q', '--query', dest='query', action='store_const', const=True,
                       help='Query destination on list of supported commands')
    parser.add_argument('-c', '--command', type=str, dest='command',
                       help='RPC command to perform')
    parser.add_argument('-a', '--arg', type=str, dest='arg', action='append',
                       help='Arguments to the RPC command in form key=val')



    args = parser.parse_args()
    if args.command is None and args.query is None:
        parser.error("either --command or --query is required")
    tmp = dict()
    for pair in args.arg or list():
        try:
            k, v = pair.split('=', 1)
            try:
                tmp[k] = json.loads(v.strip())
            except ValueError:
                tmp[k] = v.strip()
        except ValueError:
            parser.error('Invalid syntax of argument {}'.format(pair))
    return {'log-level' : args.loglevel.upper() if args.loglevel else None,
            'destination' : args.dest,
            'uri' : args.uri,
            'query' : args.query,
            'command' : args.command,
            'args' : tmp}



def main():
    args = parse_args()
    try:
        conn = RedisConnector()
        conn.connect(args['uri'])
        if args['query']:
            code, reply = conn.send_request(args['destination'], 'commands')
        else:
            code, reply = conn.send_request(args['destination'], args['command'], **args['args'])
        print(json.dumps(reply, indent=2, sort_keys=True))
        sys.exit(code)
    except RuntimeError as err:
        logging.critical(str(err))
        sys.exit(1)


if __name__ == '__main__':
    main()
